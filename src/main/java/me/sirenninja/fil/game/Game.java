package me.sirenninja.fil.game;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.utils.EventCalls;
import me.sirenninja.fil.utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Game {
    private TheFloorIsLava fil;
    private GameUtils gUtils;

    private Integer[] count = {60, 30, 10, 5};
    private int gameStartTime = 60;
    private int gameActuallyStarting = 5;
    private int seconds = 0;
    private ArrayList<Player> currentlyPlaying = new ArrayList<>();
    private ArrayList<Player> currentlyAlive = new ArrayList<>();
    private ArrayList<Player> currentlyDead = new ArrayList<>();
    private HashMap<Player, String> inventories = new HashMap<>();
    private HashMap<Player, Location> lastLocations = new HashMap<>();
    private HashMap<Long, String> kills = new HashMap<>();
    private boolean isGameRunning = false;
    private boolean isDeathMatch = false;
    private int deathMatchSeconds = 300;
    private int deathMatchTemp = 5;
    private int minPlayers = 2;
    private int tempround = 0;
    private int round = 1;
    private int maxRound = 10;

    private boolean shouldUpdate = true;

    public Game(TheFloorIsLava fil, int minPlayers, int maxRound, int deathMatchSeconds){
        this.fil = fil;

        if(gUtils == null)
            gUtils = new GameUtils(fil, this);

        this.deathMatchSeconds = deathMatchSeconds;

        this.minPlayers = minPlayers;

        this.maxRound = maxRound;

        starting();
    }

    private void resetGame(){
        gUtils.restoreFloor();

        isGameRunning = false;
        isDeathMatch = false;

        round = 1;

        currentlyAlive.clear();
        currentlyDead.clear();
        currentlyPlaying.clear();

        EventCalls.callEndEvent();
        restartServer();
    }

    public GameUtils getUtils(){
        return gUtils;
    }

    private void starting(){
        new BukkitRunnable(){

            @Override
            public void run(){
                if(isGameRunning){
                    this.cancel();
                    return;
                }

                if(currentlyAlive.size() > minPlayers){
                    Utils.sendGlobalActionBar(ChatColor.RED + "WAITING FOR " + (minPlayers - 1) + " MORE " + (minPlayers - 1 == 1 ? "PLAYER!" : "PLAYERS!"));
                    return;
                }

                if(gameStartTime > 0){
                    if(Utils.arrayContains(count, gameStartTime))

                    Utils.sendGlobalActionBar(ChatColor.GREEN + "GAME WILL START IN " + gameStartTime + (gameStartTime == 1 ? " SECOND!" : " SECONDS!"));
                }

                if(gameStartTime <= 0){
                    isGameRunning = true;

                    gUtils.sendToGameSpawn();
                    updateScoreboard();

                    EventCalls.callGameStartEvent();

                    gameStartTime = 60;
                    running();

                    this.cancel();
                    return;
                }

                gameStartTime--;
            }

        }.runTaskTimerAsynchronously(fil, 0, 20);
    }

    private void running(){
        new BukkitRunnable(){

            @Override
            public void run(){

                // Checks if the game is running.
                if(!(isGameRunning))
                    return;

                // Checks if the size is less than the minimum players.
                if(currentlyAlive.size() < minPlayers){
                    checkWinner();
                    resetGame();
                    this.cancel();
                }

                // Checks if the round is greater than the max round.
                if(round > maxRound){
                    checkWinner();
                    resetGame();
                    this.cancel();
                }

                // First 5 seconds of teleporting to the game spawn.
                if(gameActuallyStarting <= 5 && gameActuallyStarting > 0){
                    Utils.sendGlobalTitle("&c" + gameActuallyStarting + (gameActuallyStarting == 1 ? " SECOND" : " SECONDS"), "&cUNTIL FLOOR TURNS INTO LAVA!", 7, 14, 7);
                    return;
                }

                // DeathMatch stuff.
                if(isDeathMatch()){

                    // Checks if deathMatchSeconds is less than or equal to 0.
                    if(deathMatchSeconds <= 0){
                        checkWinner();
                        resetGame();
                        return;
                    }

                    // Checks if deathMatchTemp is less than 6 and greater than 0.
                    if(deathMatchTemp < 6 && deathMatchTemp >= 0){
                        Utils.sendGlobalTitle("&cDEATHMATCH - " + (deathMatchSeconds/60) + " MINUTES TO WIN!", "&c&l" + deathMatchTemp + " BEFORE THE LAVA RETURNS", 7, 14 , 7);

                        if(deathMatchTemp <= 0)
                            gUtils.setFloorToLava();

                        deathMatchTemp--;
                        return;
                    }

                    updateScoreboard();
                    deathMatchSeconds--;
                    return;
                }

                // Updates the rounds and temprounds.
                if(seconds >= 30){
                    seconds = 0;
                    tempround++;

                    if(Utils.isDivisibleBy(tempround, 2)){
                        shouldUpdate = true;
                    }else{
                        round++;
                        EventCalls.callRoundChangeEvent();
                    }
                }

                // Should notify the last 5 seconds.
                if(shouldNotify())
                    Utils.sendGlobalTitle("&a" + String.valueOf((30 - seconds)) + (seconds == 29 ? " SECOND" : " SECONDS"), (Utils.isDivisibleBy(tempround, 2) ? "&aTILL THE FLOOR RETURNS BACK TO NORMAL!" : "TILL THE FLOOR CHANGES TO LAVA!"), 7, 14, 7);

                // If the floor is should, then either change floor it lava, or restore it.
                if(shouldUpdate){
                    if(Utils.isDivisibleBy(tempround, 2)){
                        gUtils.setFloorToLava();
                        sendCampingAction(false);
                    }else{
                        gUtils.restoreFloor();
                        sendCampingAction(true);
                    }

                    shouldUpdate = false;
                    updateScoreboard();
                }

            }

        }.runTaskTimerAsynchronously(fil, 0, 20);
    }

    public boolean hasGameStarted(){
        return isGameRunning;
    }

    public ArrayList<Player> getAlive(){
        return currentlyAlive;
    }

    public ArrayList<Player> getDead(){
        return currentlyDead;
    }

    public ArrayList<Player> getCP(){
        return currentlyPlaying;
    }

    public HashMap<Player, String> getInventories(){
        return inventories;
    }

    public HashMap<Player, Location> getLastLocations(){
        return lastLocations;
    }

    public HashMap<Long, String> getKills(){
        return kills;
    }

    public int getRound(){
        return round;
    }

    public int getMaxRound(){
        return maxRound;
    }

    public int getDeathMatchSeconds(){
        return deathMatchSeconds;
    }

    public boolean isDeathMatch(){
        return (tempround >= (maxRound*2)+1);
    }

    private boolean shouldNotify(){
        return seconds >= 25 && seconds <= 30;
    }

    private void restartServer() {
        new BukkitRunnable() {

            @Override
            public void run() {
                //Bukkit.shutdown();
            }

        }.runTaskLater(fil, 10 * 20);
    }

    private void playerWon(String winner){
        Utils.sendGlobalTitle("&a" + winner.toUpperCase(), "&cHAS WON THE GAME!", 14, 21, 14);
    }

    private void noOneWon(){
        Utils.sendGlobalTitle("&cNO ONE WON THE GAME!", "", 14, 21, 14);
    }

    private void checkWinner(){
        if(currentlyAlive.size() == 1){
            playerWon(currentlyAlive.get(0).getName());
            return;
        }

        noOneWon();
    }

    private void sendCampingAction(boolean notify){
        Iterator<Map.Entry<Player, Location>> it = lastLocations.entrySet().iterator();

        if(!(it.hasNext()))
            return;

        while(it.hasNext()){
            Map.Entry<Player, Location> entry = it.next();

            Player player = entry.getKey();

            if(!(gUtils.isInRadius(player))) {
                lastLocations.replace(player, player.getLocation());
                return;
            }

            if(notify){
                Utils.sendActionBar(player, "&c&lYOU WILL BE SLAPPED IF YOU DON'T MOVE UPON NEXT LAVA FLOOR!");
            }else{
                EventCalls.callCampingEvent(player);
            }
        }

        currentlyAlive.forEach(player -> {
            if(!(lastLocations.containsKey(player)))
                lastLocations.put(player, player.getLocation());
        });
    }

    public void updateScoreboard(){
        gUtils.updateScoreboard();
    }
}
