package me.sirenninja.fil.game;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.utils.Scoreboard;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.IBlockData;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GameUtils {
    private TheFloorIsLava fil;
    private Game game;

    public GameUtils(TheFloorIsLava fil, Game game){
        this.fil = fil;
        this.game = game;
    }

    public void setFloorToLava(){
        new BukkitRunnable(){

            @Override
            public void run(){
                // set floor to lava.
                // setBlocks(block, blockid, data, physics).
            }

        }.runTaskLater(fil, 20);
    }

    public void restoreFloor(){
        new BukkitRunnable(){

            @Override
            public void run(){
                // restore floor.
            }

        }.runTaskLater(fil, 20);
    }

    public void updateScoreboard(){
        for (Player player : game.getAlive()) {
            Scoreboard.updateScoreboard(player);
        }
    }

    public boolean isInRadius(Player player){
        return !(game.getDead().contains(player)) && player.getLocation().distanceSquared(game.getLastLocations().get(player)) <= 3D * 3D;
    }

    public void sendToGameSpawn(){
        Location loc = game.getCP().get(0).getLocation();

        String[] xyz = TheFloorIsLava.getInstance().getConfig().getString("game.spawn.loc_xyz").split(",");

        loc.setX(Double.valueOf(xyz[0]));
        loc.setY(Double.valueOf(xyz[1]));
        loc.setZ(Double.valueOf(xyz[2]));
        loc.setPitch(Float.valueOf(TheFloorIsLava.getInstance().getConfig().getString("game.spawn.loc_pitch")));
        loc.setYaw(Float.valueOf(TheFloorIsLava.getInstance().getConfig().getString("game.spawn.loc_yaw")));

        for(Player p : game.getAlive())
            p.teleport(loc);
    }

    public void sendToSpectator(Player player){
        Location loc = game.getCP().get(0).getLocation();

        String[] xyz = TheFloorIsLava.getInstance().getConfig().getString("game.spectator.loc_xyz").split(",");

        loc.setX(Double.valueOf(xyz[0]));
        loc.setY(Double.valueOf(xyz[1]));
        loc.setZ(Double.valueOf(xyz[2]));
        loc.setPitch(Float.valueOf(TheFloorIsLava.getInstance().getConfig().getString("game.spectator.loc_pitch")));
        loc.setYaw(Float.valueOf(TheFloorIsLava.getInstance().getConfig().getString("game.spectator.loc_yaw")));

        player.teleport(loc);
    }

    public void sendToLobby(Player player){
        Location loc = game.getCP().get(0).getLocation();

        String[] xyz = TheFloorIsLava.getInstance().getConfig().getString("lobby.loc_xyz").split(",");

        loc.setX(Double.valueOf(xyz[0]));
        loc.setY(Double.valueOf(xyz[1]));
        loc.setZ(Double.valueOf(xyz[2]));
        loc.setPitch(Float.valueOf(TheFloorIsLava.getInstance().getConfig().getString("lobby.loc_pitch")));
        loc.setYaw(Float.valueOf(TheFloorIsLava.getInstance().getConfig().getString("lobby.loc_yaw")));

        player.teleport(loc);
    }

    // NMS. Only for 1.12, however other support will be added when I get it properly working.
    private void setBlocks(Block b, int blockID, byte data, boolean applyPhysics){
        net.minecraft.server.v1_12_R1.World w = ((CraftWorld) b.getWorld()).getHandle();
        net.minecraft.server.v1_12_R1.Chunk chunk = w.getChunkAt(b.getX() >> 4, b.getZ() >> 4);
        BlockPosition bp = new BlockPosition(b.getX(), b.getY(), b.getZ());
        int combined = blockID + (data << 12);
        IBlockData ibd = net.minecraft.server.v1_12_R1.Block.getByCombinedId(combined);
        if (applyPhysics) {
            w.setTypeAndData(bp, ibd, 3);
        } else {
            w.setTypeAndData(bp, ibd, 2);
        }
        chunk.a(bp, ibd);
    }
}
