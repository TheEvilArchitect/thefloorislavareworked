package me.sirenninja.fil.utils;

public interface IPlayerConfig {

    int getLevel();

    void addLevel(int level);

    int getEXP();

    void addEXP(int exp);

    int getCoins();

    void addCoins(int coins);

    int getKills();

    void addKills(int kills);

    int getWins();

    void addWins(int wins);
}
