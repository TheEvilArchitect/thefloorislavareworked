package me.sirenninja.fil.utils.player;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.game.Game;
import me.sirenninja.fil.game.GameUtils;
import me.sirenninja.fil.utils.IPlayerConfig;
import me.sirenninja.fil.utils.config.PlayerConfig;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.UUID;

public class FILPlayer {

    private TheFloorIsLava fil;

    private Player player;
    private IPlayerConfig config;

    private String chosenKit;

    private boolean isReady = false;

    public FILPlayer(TheFloorIsLava fil, Player player){
        this.fil = fil;
        this.player = player;

        config = new PlayerConfig(this.player, new File(TheFloorIsLava.getInstance().getDataFolder(), File.separator + "PlayerData"));

        
    }

    public Player getPlayer(){
        return this.player;
    }

    public void setKit(String kit){
        chosenKit = kit;
    }

    public String getKit(){
        return chosenKit;
    }

    public IPlayerConfig getConfig() {
        return config;
    }

    public int getLevel(){
        return config.getLevel();
    }

    public void addLevel(int level){
        config.addLevel(level);
    }

    public int getEXP(){
        return config.getEXP();
    }

    public void addEXP(int exp){
        config.addEXP(exp);
    }

    public int getCoins(){
        return config.getCoins();
    }

    public void addCoins(int coins){
        config.addCoins(coins);
    }

    public int getKills(){
        return config.getKills();
    }

    public void addKills(int kills){
        config.addKills(kills);
    }

    public int getWins(){
        return config.getWins();
    }

    public void addWins(int wins){
        config.addKills(wins);
    }

    public boolean isReady(){
        return isReady;
    }

    public void setReady(boolean ready){
        isReady = ready;
    }

    public void died(){
        if(player != null){
            player.setGameMode(GameMode.SPECTATOR);
            fil.getGame().getUtils().sendToSpectator(player);

            if(!(fil.getGame().getDead().contains(player)))
                fil.getGame().getDead().add(player);
        }

        fil.getGame().getCP().remove(player);

        fil.getGame().getInventories().remove(player);
        fil.getGame().getAlive().remove(player);
        fil.getGame().getLastLocations().remove(player);
    }
}