package me.sirenninja.fil.utils;

import me.sirenninja.fil.TheFloorIsLava;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class Utils {

    public static String getColor(String message){
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static int slapVelocity(int max, int min) {
        return -min + (int) (Math.random() * ((max - (-min)) + 1));
    }

    public static boolean arrayContains(Integer[] array, int value) {
        return value < array[array.length - 1] && value > 0 || Arrays.asList(array).contains(value);
    }

    public static boolean isDivisibleBy(int number, int n){
        return ((number % n) == 0);
    }

    public static void sendActionBar(Player player, String message){
        player.spigot().sendMessage(new TextComponent(getColor(message)));
    }

    public static void sendGlobalActionBar(String message){
        TheFloorIsLava.getInstance().getGame().getAlive().forEach(player -> player.spigot().sendMessage(new TextComponent(getColor(message))));
    }

    public static void sendTitle(Player player, String title, String subtitle, int fadeIn, int stay, int fadeOut){
        player.sendTitle(getColor(title), getColor(subtitle), fadeIn, stay, fadeOut);
    }

    public static void sendGlobalTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut){
        TheFloorIsLava.getInstance().getGame().getAlive().forEach(player -> player.sendTitle(getColor(title), getColor(subtitle), fadeIn, stay, fadeOut));
    }

    public static void serverTransfer(Player p, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            server = ChatColor.stripColor(server);
            out.writeUTF("Connect");
            out.writeUTF(server);

        } catch (IOException e) {
            e.printStackTrace();
        }
        p.sendPluginMessage(TheFloorIsLava.getInstance(), "BungeeCord", b.toByteArray());
    }

    public static ItemStack getGUI(){
        ItemStack is = new ItemStack(Material.ENDER_CHEST, 1);
        ItemMeta im = is.getItemMeta();

        im.setDisplayName(getColor("&3Kits"));
        is.setItemMeta(im);

        return is;
    }

    public static double getPossibleKnockoff() {
        int value = -1 + (int) (Math.random() * ((10 - (-1)) + 1));

        if (value > 5)
            return 0.3;

        return 0;
    }
}
