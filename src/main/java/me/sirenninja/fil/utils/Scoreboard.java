package me.sirenninja.fil.utils;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.game.Game;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

public class Scoreboard {

    public static void updateScoreboard(Player p) {
        new BukkitRunnable() {

            @Override
            public void run() {
                setScoreboard(p);
            }

        }.runTaskLater(TheFloorIsLava.getInstance(), 5);
    }

    private static void setScoreboard(Player p) {
        org.bukkit.scoreboard.Scoreboard board = TheFloorIsLava.getInstance().getServer().getScoreboardManager().getNewScoreboard();
        Objective objective = board.registerNewObjective("Scoreboard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        objective.setDisplayName(ChatColor.GRAY + "FI" + ChatColor.RED + "L" + ChatColor.GRAY);

        String[] lines = new String[]{
                "",
                ChatColor.BLUE + "Round: ",
                (TheFloorIsLava.getInstance().getGame().getRound() != (TheFloorIsLava.getInstance().getGame().getMaxRound() + 1) ? ChatColor.WHITE + "" + TheFloorIsLava.getInstance().getGame().getRound() + ChatColor.GOLD + "/" + ChatColor.WHITE + TheFloorIsLava.getInstance().getGame().getMaxRound() : ChatColor.RED + "Deathmatch") + " ",
                " ",
                ChatColor.GREEN + "Alive: ",
                "" + TheFloorIsLava.getInstance().getGame().getAlive().size() + "  ",
                "  ",
                ChatColor.RED + "Dead: ",
                "" + TheFloorIsLava.getInstance().getGame().getDead().size() + "   ",
                "   ",
                ChatColor.RED + "DeathMatch: ",
                "" + (TheFloorIsLava.getInstance().getGame().getDeathMatchSeconds() / 60) + "m " + (TheFloorIsLava.getInstance().getGame().getDeathMatchSeconds() % 60) + "s"
        };


        for (int i = 0; i < lines.length; i++) {
            Score score = objective.getScore(lines[i]);
            score.setScore(lines.length - i);
        }

        p.setScoreboard(board);
    }
}