package me.sirenninja.fil.utils;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.apievents.*;
import me.sirenninja.fil.enums.CAUSE;
import org.bukkit.entity.Player;

public class EventCalls {

    public static void callPlayerDiedEvent(Player player, CAUSE cause){
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        PlayerDiedEvent tEvent = new PlayerDiedEvent(player, cause);
        fil.getServer().getPluginManager().callEvent(tEvent);
    }

    public static void callGameStartEvent(){
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        GameStartEvent tEvent = new GameStartEvent(fil.getGame().getCP(), fil.getGame().getAlive(), fil.getGame().getDead());
        fil.getServer().getPluginManager().callEvent(tEvent);
    }

    public static void callEndEvent() {
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        GameEndEvent tEvent = new GameEndEvent(fil.getGame().getCP(), fil.getGame().getAlive(), fil.getGame().getDead());
        fil.getServer().getPluginManager().callEvent(tEvent);
    }

    public static void callRoundChangeEvent() {
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        RoundChangeEvent tEvent = new RoundChangeEvent(fil.getGame().getRound(), fil.getGame().getCP(), fil.getGame().getAlive(), fil.getGame().getDead());
        fil.getServer().getPluginManager().callEvent(tEvent);
    }

    public static void callCampingEvent(Player player) {
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        PlayerCampingEvent tEvent = new PlayerCampingEvent(player, player.getLocation());
        fil.getServer().getPluginManager().callEvent(tEvent);
    }

    public static void callLobbyEvent(Player player, Boolean state){
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        LobbyEvent tEvent = new LobbyEvent(fil.getGame().getCP(), player, state);
        fil.getServer().getPluginManager().callEvent(tEvent);
    }

    public static void callSelectedKitEvent(Player player, String kit, String kitname){
        TheFloorIsLava fil = TheFloorIsLava.getInstance();

        SelectedKitEvent tEvent = new SelectedKitEvent(player, kit, kitname);
        fil.getServer().getPluginManager().callEvent(tEvent);
    }
}
