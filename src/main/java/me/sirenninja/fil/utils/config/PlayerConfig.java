package me.sirenninja.fil.utils.config;

import me.sirenninja.fil.utils.IPlayerConfig;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class PlayerConfig implements IPlayerConfig {

    private Player player;

    private File file;
    private FileConfiguration config;

    public PlayerConfig(Player player, File root){
        this.player = player;
        this.file = root;

        if(!(root.exists()))
            root.mkdir();

        try{
            if(!(file.exists()))
                file.createNewFile();

            config = YamlConfiguration.loadConfiguration(file);

            config.set("PlayerData.username", player.getName());
            config.set("PlayerData.UUID", player.getUniqueId().toString());


            addDefault("PlayerData.coins", 0);
            addDefault("PlayerData.level", 1);
            addDefault("PlayerData.exp", 0);
            addDefault("PlayerData.kills", 0);
            addDefault("PlayerData.wins", 0);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public int getLevel(){
        return config.getInt("PlayerData.level", 1);
    }

    public void addLevel(int level) {
        set("PlayerData.level", (getLevel() + level));
    }


    public int getEXP(){
        return config.getInt("PlayerData.exp", 0);
    }

    public void addEXP(int exp){
        set("PlayerData.exp", (getEXP() + exp));
    }

    public int getCoins(){
        return config.getInt("PlayerData.coins", 0);
    }

    public void addCoins(int coins){
        set("PlayerData.coins", (getCoins() + coins));
    }

    public int getKills(){
        return config.getInt("PlayerData.kills", 0);
    }

    public void addKills(int kills){
        set("PlayerData.kills", (getKills() + kills));
    }

    public int getWins(){
        return config.getInt("PlayerData.wins", 0);
    }

    public void addWins(int wins){
        set("PlayerData.wins", (getWins() + wins));
    }

    private void set(String path, Object value){
        config.set(path, value);

        saveConfig();
    }

    private void addDefault(String path, Object value){
        if(!(config.isSet(path)))
            set(path, value);
    }

    private void saveConfig(){
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}