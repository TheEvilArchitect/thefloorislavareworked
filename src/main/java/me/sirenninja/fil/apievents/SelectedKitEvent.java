package me.sirenninja.fil.apievents;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SelectedKitEvent extends Event implements Cancellable {

    private static final HandlerList handlers  = new HandlerList();

    private Player player;
    private String kit;
    private String kitname;
    private boolean isCancelled;

    public SelectedKitEvent(Player player, String kit, String kitname){
        this.player = player;
        this.kit = kit;
        this.kitname = kitname;

        if(!(isCancelled)){
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou have selected the " + kitname + " &akit!"));
        }
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer(){
        return this.player;
    }

    public String getKit(){
        return this.kit;
    }

    public String getKitDisplayName(){
        return ChatColor.translateAlternateColorCodes('&', this.kitname);
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean state) {
        isCancelled = state;
    }
}
