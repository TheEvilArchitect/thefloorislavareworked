package me.sirenninja.fil.apievents;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.enums.CAUSE;
import me.sirenninja.fil.game.Game;
import me.sirenninja.fil.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Iterator;
import java.util.Map;

public class PlayerDiedEvent extends Event implements Cancellable {

    private static final HandlerList handlers  = new HandlerList();

    private Player player;
    private CAUSE cause;
    private boolean cancelled;

    public PlayerDiedEvent(Player player, CAUSE cause){
        this.player = player;
        this.cause = cause;

        Game game = TheFloorIsLava.getInstance().getGame();

        if(!(isCancelled())){
            TheFloorIsLava.getInstance().getPlayer(player).died();

            for(Player p : game.getCP()){
                if(TheFloorIsLava.getInstance().getGame().getAlive().size() > 1)
                    Utils.sendGlobalTitle("&c" + player.getName(), "&chas died! &f" + (game.getAlive().size() > 1 ? game.getAlive().size() + " alive left remaining!" : ""), 7, 14, 7);
            }

            Iterator<Map.Entry<Long, String>> it = game.getKills().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Long, String> entry = it.next();
                String[] players = entry.getValue().split(":");

                String killedPlayer = players[0];
                String killer = players[1];

                if(!(killedPlayer.equalsIgnoreCase(player.getName())))
                    continue;

                if(cause == CAUSE.LAVA)
                    Bukkit.broadcastMessage(Utils.getColor("&c" + player.getName() + " was killed by &l" + killer + "&c from &lLAVA&c! &f" + (game.getAlive().size() > 1 ? game.getAlive().size() + " alive left remaining!" : "")));
                else if(cause == CAUSE.SNOWBALL)
                    Bukkit.broadcastMessage(Utils.getColor("&c" + player.getName() + " was killed by &l" + killer + "&c by using a &lSNOWBALLS&c! &f" + (game.getAlive().size() > 1 ? game.getAlive().size() + " alive left remaining!" : "")));

                Player p = TheFloorIsLava.getInstance().getServer().getPlayer(killer);

                // TODO: Add this to the FILPlayer class, for simplicity.
                if(p != null) {
                    TheFloorIsLava.getInstance().getPlayer(p).addCoins(25);
                    TheFloorIsLava.getInstance().getPlayer(p).addEXP(25);
                    TheFloorIsLava.getInstance().getPlayer(p).addKills(1);
                }

                    it.remove();
                    return;
            }
        }
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer(){
        return this.player;
    }

    public CAUSE getCause(){
        return this.cause;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean state) {
        this.cancelled = state;
    }
}
