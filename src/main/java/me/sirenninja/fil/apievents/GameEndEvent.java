package me.sirenninja.fil.apievents;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;

public class GameEndEvent extends Event{
	
private static final HandlerList handlers  = new HandlerList();

	private ArrayList<Player> currentlyPlaying;
	private ArrayList<Player> alivePlayers;
	private ArrayList<Player> deadPlayers;
	
	public GameEndEvent(ArrayList<Player> currentlyPlaying, ArrayList<Player> alivePlayers, ArrayList<Player> deadPlayers){
		this.currentlyPlaying = currentlyPlaying;
		this.alivePlayers = alivePlayers;
		this.deadPlayers = deadPlayers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	public ArrayList<Player> getCurrentlyPlaying(){
		return this.currentlyPlaying;
	}
	
	public ArrayList<Player> getAlivePlayers(){
		return this.alivePlayers;
	}
	
	public ArrayList<Player> getDeadPlayers(){
		return this.deadPlayers;
	}
}