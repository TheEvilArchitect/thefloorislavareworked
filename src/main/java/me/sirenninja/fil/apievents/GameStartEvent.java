package me.sirenninja.fil.apievents;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;

public class GameStartEvent extends Event{
	
	private static final HandlerList handlers  = new HandlerList();

	private ArrayList<Player> currentlyPlaying;
	private ArrayList<Player> aliveList;
	private ArrayList<Player> deadList;

	public GameStartEvent(ArrayList<Player> currentlyPlaying, ArrayList<Player> aliveList, ArrayList<Player> deadList){
		this.currentlyPlaying = currentlyPlaying;
		this.aliveList = aliveList;
		this.deadList = deadList;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	public ArrayList<Player> getCurrentlyPlaying(){
		return this.currentlyPlaying;
	}

	public ArrayList<Player> getAliveList(){
		return this.aliveList;
	}

	public ArrayList<Player> getDeadList() {
		return this.deadList;
	}
}
