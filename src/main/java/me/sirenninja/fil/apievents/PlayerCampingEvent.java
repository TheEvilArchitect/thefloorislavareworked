package me.sirenninja.fil.apievents;

import me.sirenninja.fil.utils.Utils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;

public class PlayerCampingEvent extends Event implements Cancellable {

    private static final HandlerList handlers  = new HandlerList();

    private Player player;
    private Location location;

    private boolean cancelled;

    public PlayerCampingEvent(Player player, Location location){
        this.player = player;
        this.location = location;

        if(!(cancelled)){
            Vector slap = player.getVelocity().setY(Utils.slapVelocity(1, 0)).setX(Utils.slapVelocity(1, 0)).setZ(Utils.slapVelocity(1, 0));
            player.setVelocity(slap);
            player.damage(1);
        }
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer(){
        return this.player;
    }

    public Location getLocation(){
        return this.location;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean state) {
        this.cancelled = state;
    }
}
