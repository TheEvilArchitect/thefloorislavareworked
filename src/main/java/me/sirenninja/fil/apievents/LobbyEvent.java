package me.sirenninja.fil.apievents;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;

public class LobbyEvent extends Event{

    private static final HandlerList handlers  = new HandlerList();

    private ArrayList<Player> currentlyPlaying;
    private Player player;
    private Boolean hasGameStarted;

    public LobbyEvent(ArrayList<Player> currentlyPlaying, Player player, Boolean hasGameStarted){
        this.currentlyPlaying = currentlyPlaying;
        this.player = player;
        this.hasGameStarted = hasGameStarted;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public ArrayList<Player> getCurrentlyPlaying(){
        return this.currentlyPlaying;
    }

    public Player getPlayer(){
        return this.player;
    }

    public Boolean hasGameStarted(){
        return this.hasGameStarted;
    }
}
