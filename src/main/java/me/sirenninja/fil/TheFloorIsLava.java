package me.sirenninja.fil;

import me.sirenninja.fil.events.CustomEvents;
import me.sirenninja.fil.events.PlayerEvents;
import me.sirenninja.fil.events.RedundantEvents;
import me.sirenninja.fil.game.Game;
import me.sirenninja.fil.utils.player.FILPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TheFloorIsLava extends JavaPlugin {

    /**
     * TODO:
     * Add the KitUtils stuff.
     * Add the last remaining events. (RoundChangeEvent, EntityDamageByEntityEvent, EntityDamageEvent, InventoryClickEvent, PlayerInteractEvent)
     * Add the commands.
     * Done?
     */

    private static TheFloorIsLava instance;
    private static Game game;

    private Map<Player, FILPlayer> players = new HashMap<>();

    private String fallbackServer = "spawn";

    @Override
    public void onEnable(){
        instance = this;

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        entries();

        getServer().getPluginManager().registerEvents(new PlayerEvents(this), this);
        getServer().getPluginManager().registerEvents(new CustomEvents(this), this);
        getServer().getPluginManager().registerEvents(new RedundantEvents(this), this);
    }

    @Override
    public void onDisable(){
        instance = null;
        players.clear();
    }

    private void entries(){
        new BukkitRunnable(){

            @Override
            public void run(){
                Iterator<Map.Entry<Long,String>> it = getGame().getKills().entrySet().iterator();

                if(!(it.hasNext()))
                    return;

                while(it.hasNext()){
                    Map.Entry<Long, String> entry = it.next();
                    long timer = (entry.getKey()/1000 + 10) - System.currentTimeMillis()/1000;

                    if(timer <= 0){
                        it.remove();
                    }
                }
            }

        }.runTaskTimer(this, 0, 20);
    }

    public FILPlayer getPlayer(Player player){
        return players.get(player);
    }

    public void addPlayer(Player player){
        if(!(players.containsKey(player)))
            players.put(player, new FILPlayer(instance, player));
    }

    public void removePlayer(Player player){
        players.remove(player);
    }

    public int getMaxPlayerSize(){
        return getConfig().getInt("global.maxPlayers", 12);
    }

    public String getFallbackServer(){
        return fallbackServer;
    }

    public static TheFloorIsLava getInstance(){
        return instance;
    }

    public Game getGame(){
        if(game == null)
            game = new Game(instance, 2, 10, 300);

        return game;
    }
}