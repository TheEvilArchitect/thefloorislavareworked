package me.sirenninja.fil.events;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.apievents.GameEndEvent;
import me.sirenninja.fil.apievents.GameStartEvent;
import me.sirenninja.fil.apievents.LobbyEvent;
import me.sirenninja.fil.utils.Utils;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class CustomEvents implements Listener {
    private TheFloorIsLava fil;

    public CustomEvents(TheFloorIsLava fil){
        this.fil = fil;
    }

    @EventHandler
    public void onPlayerJoinLobby(LobbyEvent event){
        event.getPlayer().setHealth(event.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());

        event.getPlayer().setFoodLevel(20);
        event.getPlayer().getInventory().clear();

        if(!(event.hasGameStarted())){
            event.getPlayer().getInventory().setItem(0, Utils.getGUI());
        }
    }

    @EventHandler
    public void onGameStart(GameStartEvent event) {
        for(Player player : event.getAliveList()){
            player.getInventory().clear();

            // GIVE KIT.
            //KitUtils.givePlayerKit(player);
        }
    }

    @EventHandler
    public void onGameEnd(GameEndEvent event) {
        for(Player player : event.getCurrentlyPlaying()){
            player.getInventory().clear();
        }

        new BukkitRunnable() {

            @Override
            public void run() {
                for(Player player : fil.getServer().getOnlinePlayers()){
                    Utils.serverTransfer(player, fil.getFallbackServer());
                }
            }

        }.runTaskLater(fil, 5 * 20);
    }
}
