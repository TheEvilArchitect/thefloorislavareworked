package me.sirenninja.fil.events;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.enums.CAUSE;
import me.sirenninja.fil.game.Game;
import me.sirenninja.fil.game.GameUtils;
import me.sirenninja.fil.utils.EventCalls;
import me.sirenninja.fil.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class PlayerEvents implements Listener {
    private TheFloorIsLava fil;

    public PlayerEvents(TheFloorIsLava fil){
        this.fil = fil;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        event.setJoinMessage(null);

        if(fil.getGame().hasGameStarted()){
            player.kickPlayer(Utils.getColor("&cGame has already started!"));
            return;
        }

        Bukkit.broadcastMessage(Utils.getColor("&3" + event.getPlayer().getName() + " &ahas joined! (" + Bukkit.getOnlinePlayers().size() + "/" + fil.getMaxPlayerSize() + ")"));

        new BukkitRunnable() {

            @Override
            public void run() {
                player.sendMessage(Utils.getColor("&aUse &3/ready &ato ready up!"));
            }

        }.runTaskLater(fil, 2 * 20);

        fil.getGame().getAlive().add(player);
        fil.getGame().getCP().add(player);
        fil.addPlayer(player);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event){
        event.setQuitMessage(null);

        fil.removePlayer(event.getPlayer());

        if(fil.getGame().hasGameStarted())
            fil.getGame().updateScoreboard();
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event){
        if(event.getFrom().getX() == event.getTo().getX() || event.getFrom().getZ() == event.getTo().getZ() || event.getFrom().getY() == event.getTo().getY())
            return;

        Player player = event.getPlayer();
        Block block = player.getLocation().subtract(0, 1, 0).getBlock();

        if ((block.getType().equals(Material.STATIONARY_LAVA)) || (block.getType().equals(Material.LAVA))) {
            if(fil.getGame().getAlive().contains(player)){
                new BukkitRunnable(){

                    @Override
                    public void run() {
                        if ((block.getType().equals(Material.STATIONARY_LAVA)) || (block.getType().equals(Material.LAVA)))
                            EventCalls.callPlayerDiedEvent(player, CAUSE.LAVA);
                    }
                }.runTaskLater(fil, 10);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        if(!(event.getPlayer().getGameMode() == GameMode.SPECTATOR) || !(fil.getGame().getDead().contains(player)))
            return;

        event.setFormat(String.format(event.getFormat(), Utils.getColor("&c[DEAD] ") + event.getPlayer().getDisplayName(), Utils.getColor("&7" + ChatColor.stripColor(event.getMessage()))));
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().startsWith("/hub") || event.getMessage().startsWith("/ready") || event.getMessage().startsWith("/stats") || event.getPlayer().hasPermission("thefloorislava.moderation"))
            return;

        List<String> allowedCommands = fil.getConfig().getStringList("global.allowedcommands");

        for (String s : allowedCommands) {
            if (event.getMessage().toLowerCase().startsWith(s))
                return;
        }

        event.setCancelled(true);
    }
}
