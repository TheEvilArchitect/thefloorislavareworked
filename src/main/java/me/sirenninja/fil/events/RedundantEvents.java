package me.sirenninja.fil.events;

import me.sirenninja.fil.TheFloorIsLava;
import me.sirenninja.fil.game.Game;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

public class RedundantEvents implements Listener {
    private TheFloorIsLava fil;

    public RedundantEvents(TheFloorIsLava fil){
        this.fil = fil;
    }

    @EventHandler
    public void placeBlock(BlockPlaceEvent event){
        if(!(event.getPlayer().getGameMode() == GameMode.CREATIVE))
            event.setCancelled(true);
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent event){
        if(!(event.getPlayer().getGameMode() == GameMode.CREATIVE))
            event.setCancelled(true);
    }

    @EventHandler
    public void foodChangeEvent(FoodLevelChangeEvent event){
        event.setFoodLevel(20);
        event.setCancelled(true);
    }

    @EventHandler
    public void onGeneration(BlockFromToEvent event){
        if((event.getBlock().getType() == Material.WATER || event.getBlock().getType() == Material.STATIONARY_WATER) && (event.getToBlock().getType() == Material.LAVA || event.getToBlock().getType() == Material.STATIONARY_LAVA))
            event.setCancelled(true);
    }

    @EventHandler
    public void diedEvent(PlayerDeathEvent event){
        event.setDeathMessage(null);
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerCraft(PrepareItemCraftEvent event) {
        event.getInventory().setResult(new ItemStack(Material.AIR));
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        if (event.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) {
            event.setCancelled(true);
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(!(fil.getGame().isDeathMatch()))
            return;

        Player player = event.getPlayer();
        if (player.getItemInHand().getType() == Material.SNOW_BALL)
            player.getInventory().setItem(4, new ItemStack(Material.SNOW_BALL, 64));
    }
}
